# Welcome to Saucerer

[Saucerer]() is a simple web scrapper for [SauceNAO](https://saucenao.com/) as an API written in Python.

## Installation

Saucerer is not available on pypi, so you'll need to install it through `git`

+ To install latest release

```bash
pip install -U git+https://github.com/teppyboy/saucerer@v0.2.0
```

+ To install latest git commit

```bash
pip install -U git+https://github.com/teppyboy/saucerer
```

## Quick start

To get started with Saucerer, check out this example code:

```python
from saucerer import Saucerer
import asyncio
# Either using async with
async with Saucerer() as saucerer:
    # search for sauce from an image file
    search_result = saucerer.search(file="./images/100980966_p0.jpg")
    # print the first sauce url
    print(search_result[0].sauce.url)
# or initialize & close it manually
async def main():
    saucerer = Saucerer()
    # search for sauce from an url
    search_result = await saucerer.search("https://ayayaxyz.tretrauit.repl.co/pixiv/raw?url=https:/i.pximg.net/img-original/img/2022/09/04/04/53/47/100980966_p0.jpg")
    await saucerer.close()
    # print the first sauce url
    print(search_result[0].sauce.url)
asyncio.run(main())
```

## Differences with other projects

Unlike [other SauceNAO API wrappers](https://pypi.org/search/?q=saucenao) which uses SauceNAO API directly, this project scrapes SauceNAO website instead, thus removing the need of token.

## CLI

Soon:tm:

## License

Saucerer is licensed under MIT License, while this documentation is licensed under CC0-1.0
