# API Documentation

## class **Saucerer**

The main class for the library.

## Methods

### Saucerer.**search**(*file: PathLike | str = None, image_url: str = None, databases: list[int] = None*) -> *list[SearchResult]*

Search image either from a `file` or an `image_url` with optionally specified `databases` matching to SauceNAO API.
> In this method you *must* specify either a `file` or an `image_url` or it'll raise an error.

## class **SearchResult**(*hidden: bool, image_url: str, image_title: str, match_percentage: float, sauce_url: str, sauce_id: int, user_url: str, user_name: str*)

An image search result

## Methods and properties

### SearchResult.**hidden** -> *bool*

Whether the image is hidden from search result or not (usually low similarity images)

### SearchResult.**image_url** -> *str*

Returns the image url 

### SearchResult.**match_percentage** -> *float*

The image match percentage to the searched image from 0 to 1

### SearchResult.**sauce** -> *Sauce*

Returns a `Sauce` object containing information about the image

### classmethod SearchResult.**from_html**(*html: BeautifulSoup*) -> *SearchResult*

Parse an `BeautifulSoup` object into a `SearchResult` object.

This method is meant to be used internally by **`Saucerer.search`**, most of the time you don't need to use this method.

## class **Sauce**(*name: str, url: str, sauce_id: int, user_name: str, user_url: str*)

A sauce image information class

## Methods and properties

### Sauce.**name** -> *str*

Returns the image name

### Sauce.**url** -> *str*

Returns the image url (not direct url). For direct url you'll want to use `SearchResult.image_url` instead

### Sauce.**id** -> *id*

Returns the image id in the provider host (e.g 100980966 in Pixiv)

### Sauce.**user** -> *SauceUser*

Returns the image author as a `SauceUser` object

